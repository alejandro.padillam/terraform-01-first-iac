variable "region" {
  description = "Location"
  type        = string
  default     = "us-central1"
}
