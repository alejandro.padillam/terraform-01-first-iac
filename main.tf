terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.56.0"
    }
  }
}

provider "google" {
  project = "vast-art-304421"
  region  = var.region
}

resource "google_compute_firewall" "default" {
  name    = "test-firewalltwo"
  network = "projects/vast-art-304421/global/default"

  allow {
    protocol = "icmp"
  }
}
